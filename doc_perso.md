# Documentation Laravel

Auteur : Sascha Sallès

## Comprendre l'architecture MVC 

![mvc](/mvc.png "mvc")

## Créer un projet

#### Avec Composer : 
`composer create-project --prefer-dist laravel/laravel blog`

## Architecture de fichiers : 
Explication de la structure d'un projet laravel
### Fichiers de base : 

* `server.php` -> server embarqué
* `artisan` -> lancé lorsqu'on fait un php artisan

* `composer.json` / composer.lock -> gestionnaire de dépendences (sommaire du projet), la où on installe des librairies
* les `vendor` : dossier dans lequel sont posés les librairies.
* require : en prod
* require-dev (pour les dev): en dev

* fichier `.env`
    * là ou l'on met les variables d'environnement du projet. (lien a la bdd etc..), utilisé uniquement en dev
* fichier `.env.example` : 
    * simple copie du `.env` -> avec tout les paramètres par defaut
* package.json -> pour les packages nodes
* webpack.mix.js -> utiliser pour minifier le code js/css (vire les commentaires etc ... statics)

### Dossiers :

`/app` -> **racine de l'app**
* console : creer des cmd artisan customs  
* exeption : traiter des exeption par nous meme
* providers : ??
		
`/bootstrap` 
* Pas utile pour nous

`/config`
* là où l'on configure les **services php** (genre fonction mail, files d'attentes...)

`/database` : **relatif à la bdd**
* factories : un factorie c'est un moule, but créer des faux users, pas en prod
* model : objet
		
`/public` : 
* point d'entrée de l'app, s'occupe de rediriger les urls

`/ressources` 
* la où sont les fichiers de langues.
* dedans il y a les **views** : exemple : `welcome.blade.php`

`/storages`
* logs de l'app, et fichiers que l'on stocke dans l'app (csv, img...)

`/routes`
* api : utiliser pour faire / utiliser des APIs (retourne des données)
* channels (pour les sockets), console (créer des cmd custom)
* fichier web (là ou tu retournes les views)

Le fichier de `migrations` communique avec la bdd, c'est lui qui créer les tables et champs de la bdd
Par défaut dans laravel il y a déja 3 fichiers de migrations.

**En termes général pour faire quelque chose :**
* `php artisan make:quelquechose`  

* **Exemple de commandes couramment utilisées :**
    * `php artisan make:controller`
    * `php artisan:make model` 
    * `php artisan migrate` : fais les migrations, ou fait la derniere migration 
    * `php artisan migrate:refresh` , vide la table de migration et relance les migrations (⚠️on perd les données)
    * `php artisan:reset`, supprime tout 

## Création d'un projet de A à Z
	
### Créer les premières pages :

#### Routes
Dans la plupart des applications nous définirons les routes dans le fichier `routes/web.php` 
Ces routes ont le groupe middleware `web`
Par exemple pour cette url :
* **http://your-app.test/user**
    * Il faut écrire : 
    `Route::get('/user', 'UserController@index');`

#### Controllers
Un controller est une classe qui traite les actions de l'utilisateur, modifie les données du modèle et de la vue.
Créer un controller :
`php artisan make:controller`

#### Views
Les views sont stockés dans `resources/views`
Une vue termine tout le temps par `.blade.php`

#### Creer un UserTableSeeder
Laravel inclus un seeder par defaut.
Toutes les classes seed se trouvent dans `database/seeds`
`php artisan make:seeder UsersTableSeeder`

Pour modifier l'assignement par defaut, on utilise la fonction `run` de la classe UserTableSeeder.
**Exemple**
```
 public function run()
    {
        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('password'),
        ]);
    }
```

Pour vérifier la connexion, le mdp par défaut est `password`.

#### Ajout de rôles

* Installer le package suivant avec la cmd `composer require spatie/laravel-permission`

* Publier la migration : `php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="migrations"`

* Une fois la migration publiée, on peux ajouter des rôles en lançant une simple migration : `php artisan migrate`

* Publier le fichier de configuration
`php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="config"`

* Créer un rôle dans notre model :
```
use Spatie\Permission\Models\Role;

$role = Role::create(['name' => 'writer']);
```

#### Associer nos user seeders avec des rôles 
**Exemple**

Créer :
* 10 users avec le premier rôle 
* 40 users avec le deuxième rôle
* 50 users avec le troisième rôle

Etape 1 : Ajouter un model UserTableSeeder
Etape 2 : Exemple d'attribution :
```
<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        factory(App\User::class, 10)->create()->each(function ($user){
            $user->assignRole("writer");
        });
        factory(App\User::class, 40)->create()->each(function ($user){
            $user->assignRole("admin");
        });
        factory(App\User::class, 50)->create()->each(function ($user){
            $user->assignRole("user");
        });
    }
};
```


#### Utilisation des middlewares 

Gardons notre exemple,
* Créer trois contrôler qui portent le nom des trois rôles que vous avez créé
(Utiliser la cmd dédiée)
* Créer trois vues que vous retournerez dans l'index de 3 controller (une vue par index de controller)
* Créer trois routes qui permettent d'accéder à ces 3 vues en fonction des 3 roles (un roles ne peut accéder qu'à une vue)

Ce qui nous interresse vraiment ce passe au niveau des routes.

Dans `web.php`
```
$routeMiddleware = [
    'role' => \Spatie\Permission\Middlewares\RoleMiddleware::class,
];
```

On assigne chaque rôle à une unique vue en fonctions des rôles :
```
Route::group(['middleware' => ['role:admin']], function () {
    Route::get('/admin', 'AdminController@index');
});

Route::group(['middleware' => ['role:user']], function () {
    Route::get('/user', 'UserController@index');
});

Route::group(['middleware' => ['role:writer']], function () {
    Route::get('/writer', 'WriterController@index'
```

A partir de cette base on peux quasiment tout faire dans le cadre de notre cours.